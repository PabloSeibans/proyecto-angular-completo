import { AfterViewInit, Component, NgZone, OnDestroy } from '@angular/core';
import { Router } from '@angular/router';
import { FormBuilder, Validators } from '@angular/forms';
import { UsuarioService } from 'src/app/services/usuario.service';
import Swal from 'sweetalert2';
import { LoginForm } from '../../interfaces/login-form.interface';

declare var google: any;

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: [ './login.component.css' ]
})
export class LoginComponent implements AfterViewInit, OnDestroy {



  public formSubmitted = false;

  public loginForm = this.fb.group({
    email: [localStorage.getItem('email') || '', [Validators.required, Validators.email] ],
    password: ['123456', Validators.required ],
    remember: [false, Validators.required]
  });


  constructor(private router: Router,
              private fb: FormBuilder,
              private usuarioService: UsuarioService,
              private ngZone: NgZone) {
              }

  ngAfterViewInit(): void {
    google.accounts.id.disableAutoSelect();
    google.accounts.id.initialize({
      //Esto lo cambie por mi id de google
      // client_id: "236025958894-l05tha7iovc0ool81upch4i6gi91npe8.apps.googleusercontent.com",
      client_id: "574602762860-h93ttcdj78s17gnc97kcrf0olhf1q1ur.apps.googleusercontent.com",
      callback: (response: any) => this.handleGoogleSignIn(response),
    });

    google.accounts.id.renderButton(
      document.getElementById("buttonDiv"),
      { size: "large", type: "standard", shape: "rectangular" }  // customization attributes
    );
  }

  login(){
    // console.log(this.loginForm.value);
    // this.router.navigateByUrl('/');

    let form: LoginForm = {
      email: this.loginForm.value.email!,
      password: this.loginForm.value.password!,
      remember: this.loginForm.value.remember!
    }

    // this.usuarioService.login(this.loginForm.value)
    this.usuarioService.login(form)
    .subscribe(
      {
        next: (resp) => {
          this.router.navigateByUrl('/dashboard');
        },
        error: (err) => {
          //Si sucede un Error
          // console.warn(err.error.msg)
          Swal.fire('Error', err.error.msg,'error');
        },
        complete: () => {
          console.log('Completado Papu :V');
        }
      });
  }


  handleGoogleSignIn(response: any) {
    console.log(response.credential);

    this.usuarioService.loginGoogle( response.credential )
            .subscribe(
              {
                next: (resp) => {
                  this.ngZone.run(() => {
                  this.router.navigateByUrl('/dashboard')})
                },
                error: (err) => console.log('La ruta con Google esta mal'),
              }
            );
    


    // This next is for decoding the idToken to an object if you want to see the details.
    let base64Url = response.credential.split('.')[1];
    let base64 = base64Url.replace(/-/g, '+').replace(/_/g, '/');
    let jsonPayload = decodeURIComponent(atob(base64).split('').map(function (c) {
      return '%' + ('00' + c.charCodeAt(0).toString(16)).slice(-2);
    }).join(''));
    console.log(JSON.parse(jsonPayload));
  }

  ngOnDestroy(): void {
    console.log('SE desactivo el inicio automatico de google');
    
    //Estoy usando esto de google pero al parecer no sirve 
    google.accounts.id.disableAutoSelect();
  }
}

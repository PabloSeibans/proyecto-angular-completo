import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class SettingsService {

  private linkTheme = document.querySelector('#theme');

  constructor() {
    const url = localStorage.getItem('theme') || './assets/css/colors/default-dark.css';
    // const url = `./assets/css/colors/${theme}.css`;

    this.linkTheme?.setAttribute('href', url);
    // ./assets/css/colors/default-dark.css
    
  }

  changeTheme( theme: string ) {
    const url = `./assets/css/colors/${theme}.css`;

    this.linkTheme?.setAttribute('href', url);
    localStorage.setItem('theme', url);
    this.checkCurrentTheme();
  }



  checkCurrentTheme(){

    // no deberia hacerse de esta manera porque salto al dom cada vez que
    // llamo esta funcion pero como es poco sirve
  const links = document.querySelectorAll('.selector');


    links.forEach(elem => {
      elem.classList.remove('working');
      const btnTheme = elem.getAttribute('data-theme');

      const btnThemeUrl = `./assets/css/colors/${btnTheme}.css`;

      const currentTheme = this.linkTheme?.getAttribute('href');

      if (btnThemeUrl === currentTheme ) {
        elem.classList.add('working')
      }
    })
    
  }
}

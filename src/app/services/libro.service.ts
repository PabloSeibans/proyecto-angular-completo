import { environment } from 'src/environments/environment';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { map } from 'rxjs';
import { Libro } from '../models/libro.model';


const base_url = environment.base_url;

@Injectable({
  providedIn: 'root'
})
export class LibroService {

  constructor(private http: HttpClient) { }


  get token(): string{
    return localStorage.getItem('token') || '';
  }

  get headers(){
    return {
      headers: {
        'x-token': this.token
      }
    }
  }

  cargarLibros(){
    const url = `${base_url}/libros`
    return this.http.get<any>(url, this.headers)
        .pipe(
          map( (resp: {
            ok: boolean,
            libros: Libro[]
          }) => resp.libros )
        )
  }

  obtenerLibroporId(id: string){
    const url = `${base_url}/libros/${id}`
      return this.http.get<any>(url, this.headers)
        .pipe(
          map( (resp: {
            ok: boolean,
            libro: Libro
          }) => resp.libro )
        )
  }

  crearLibro( libro: {nombre: string, hospital: string}){
    const url = `${base_url}/libros`
    return this.http.post(url, libro,this.headers);
  }

  actualizarLibro( libro: Libro){
    const url = `${base_url}/libros/${libro._id}`
    return this.http.put(url,libro,this.headers);
  }

  eliminarLibro( _id: string){
    const url = `${base_url}/libros/${_id}`
    return this.http.delete(url,this.headers);
  }
}

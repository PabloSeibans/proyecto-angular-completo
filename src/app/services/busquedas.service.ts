import { Libro } from 'src/app/models/libro.model';
import { Usuario } from './../models/usuarios.model';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { map } from 'rxjs';
import { environment } from 'src/environments/environment';
import { Hospital } from '../models/hospital.model';

const base_url = environment.base_url;

@Injectable({
  providedIn: 'root'
})
export class BusquedasService {


  constructor(private http: HttpClient) { }

  get token(): string{
    return localStorage.getItem('token') || '';
  }

  get headers(){
    return {
      headers: {
        'x-token': this.token
      }
    }
  }

  private transformarUsuarios(resultados: any[]): Usuario[]{
    return resultados.map(
      user => new Usuario(user.google,user.nombre, user.email, user.img, '', user.role, user.uid)
    );
  }

  private transformarHospitales(resultados: any[]): Hospital[]{
    return resultados;
  }

  private transformarLibros(resultados: any[]): Libro[]{
    return resultados;
  }

  buscarTodo(termino: string){
    const url = `${base_url}/todo/${termino}`
    return this.http.get(url, this.headers)
  }

  buscarU(
    tipo: 'usuarios'|'libros'|'hospitales',
    termino: string
    ){
    //TODO se puede implementar el paginado de la búsqueda pero hace falta en el backend
    //?desde=${desde}
    const url = `${base_url}/todo/coleccion/${tipo}/${termino}`
    return this.http.get<any[]>(url, this.headers).pipe(
          map((resp: any) => {
            switch (tipo) {
              case 'usuarios':
                return this.transformarUsuarios(resp.resultados);
              // case 'hospitales':
              //   return this.transformarHospitales(resp.resultados);
              default:
                return [];
            }
          })
        )
  }


  buscarH(
    tipo: 'usuarios'|'libros'|'hospitales',
    termino: string
    ){
    //TODO se puede implementar el paginado de la búsqueda pero hace falta en el backend
    //?desde=${desde}
    const url = `${base_url}/todo/coleccion/${tipo}/${termino}`
    return this.http.get<any[]>(url, this.headers).pipe(
          map((resp: any) => {
            switch (tipo) {
              case 'hospitales':
                return this.transformarHospitales(resp.resultados);
              default:
                return [];
            }
          })
        )
  }

  buscarL(
    tipo: 'usuarios'|'libros'|'hospitales',
    termino: string
    ){
    //TODO se puede implementar el paginado de la búsqueda pero hace falta en el backend
    //?desde=${desde}
    const url = `${base_url}/todo/coleccion/${tipo}/${termino}`
    return this.http.get<any[]>(url, this.headers).pipe(
          map((resp: any) => {
            switch (tipo) {
              case 'libros':
                return this.transformarLibros(resp.resultados);
              default:
                return [];
            }
          })
        )
  }

}

import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class SidebarService {

  public menu: [] = [];

  // menu: any[] = [
  //   {
  //     titulo: 'Principal',
  //     icon: 'mdi mdi-gauge',
  //     submenu: [
  //       {titulo: 'Main', url: '/'},
  //       {titulo: 'Gráficas', url: 'grafica1'},
  //       {titulo: 'Rxjs', url: 'rxjs'},
  //       {titulo: 'Promesas', url: 'promesas'},
  //       {titulo: 'ProgresBar', url: 'progress'},
  //     ]
  //   },
  //   {
  //     titulo: 'Mantenimiento',
  //     icon: 'mdi mdi-folder-lock-open',
  //     submenu: [
  //       {titulo: 'Usuarios', url: 'usuarios'},
  //       {titulo: 'Hospitales', url: 'hospitales'},
  //       {titulo: 'Libros', url: 'libros'},
  //     ]
  //   }
  // ]

  constructor() {
    this.menu = JSON.parse(localStorage.getItem('menu')!) || [];
  }

  cargarMenu(){
    this.menu = JSON.parse(localStorage.getItem('menu')!) || [];
  }
}

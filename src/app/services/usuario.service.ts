import { tap, map, catchError } from 'rxjs/operators';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Observable, of } from 'rxjs';

import { environment } from '../../environments/environment';

import { RegisterForm } from '../interfaces/register-form.interface';
import { LoginForm } from '../interfaces/login-form.interface';
import { CargarUsuarios } from '../interfaces/cargar-usuarios.interface';

import { Usuario } from '../models/usuarios.model';


const base_url = environment.base_url;

@Injectable({
  providedIn: 'root'
})
export class UsuarioService {

  // public google: any;
  public usuario!: Usuario;

  constructor(
    public http: HttpClient,
    private router: Router,
  ) {
    console.log('Servicio de Usuario Listo');
  }

  get token(): string{
    return localStorage.getItem('token') || '';
  }

  get uid(): string{
    return this.usuario.uid || '';
  }

  get headers(){
    return {
      headers: {
        'x-token': this.token
      }
    }
  }

  get role(): 'ADMIN_ROLE' | 'USER_ROLE' {
    return this.usuario.role!;
  }

  guardarLocalStorage(token: string, menu: any){
    localStorage.setItem('token', token);
    localStorage.setItem('menu', JSON.stringify(menu));
  }
 


  validarToken(): Observable<boolean> {

    return this.http.get(`${base_url}/login/renew`, {
      headers: {
        'x-token': this.token
      }
    }).pipe(
      map((resp: any) => {

          const {email,google,img = '',nombre,role,uid,} = resp.usuario;

          
          this.usuario = new Usuario(google, nombre,email,img,'',role,uid);
          //En este punto recuperamos la info y esto se ejecuta antes
          //de ingresar a la ruta dashboard
          this.usuario.imprimirUsuario();
          this.guardarLocalStorage(resp.token, resp.menu)
          return true;
        }),
        //Este catch error atrapa el error del flujo y of devuelve
        //un nuevo Observable
        catchError(error => of(false))
    );
  }


  crearUsuario(formData: RegisterForm) {
    return this.http.post(`${base_url}/usuarios`, formData)
      .pipe(
        tap((resp: any) => {
          this.guardarLocalStorage(resp.token, resp.menu)
        })
      )
  }

  actualizarPerfil(data:{email: string, nombre: string, role: string}){


    //TODO el rol lo estaba extrayendo para que no se cambie
    data = {
      ...data,
      role: this.usuario.role!
    };

    return this.http.put(`${base_url}/usuarios/${this.uid}`, data, this.headers);
  }

  login(formDate: LoginForm){

    if (formDate.remember) {
      localStorage.setItem('email', formDate.email);
    } else {
      localStorage.removeItem('email');
    }

    return this.http.post(`${base_url}/login`, formDate)
      .pipe(
        tap((resp: any) => {
          //TODO Lo que esta comentado no esta en el codigo final de Fernando
          // localStorage.setItem('id', resp.usuarioDB.uid);
          this.guardarLocalStorage(resp.token, resp.menu)

          // localStorage.setItem('usuarioDB', JSON.stringify(resp.usuarioDB));
          // return true;
        }));

        //TODO No estoy seguro de implementar esto
        //,catchError(error => of(false))
  }

  loginGoogle(token: any){

    return this.http.post(`${base_url}/login/google`, { token: token })
      .pipe(
        tap((resp: any) => {
          this.guardarLocalStorage(resp.token, resp.menu)
        }));
  }

  logout(){
    localStorage.removeItem('token');
    localStorage.removeItem('menu');
    // TODO BORRAR MENU 
    this.router.navigateByUrl('/login');
  }

  cargarUsuarios(desde: number = 0){
    const url = `${base_url}/usuarios?desde=${desde}`
    return this.http.get<CargarUsuarios>(url, this.headers)
          .pipe(
            map(resp => {
              const usuarios = resp.usuarios.map(
                //TODO ACA ESTAMOS INSTANCIANDO LA CLASE
                user => new Usuario(user.google,user.nombre, user.email, user.img, '', user.role, user.uid)
                );
              return { 
                total: resp.total,
                usuarios
              };
            })
          )
  }

  eliminarUsuario( usuario: Usuario ){
    const url = `${base_url}/usuarios/${usuario.uid}`
    return this.http.delete(url, this.headers);
  }

  cambiarUsuario(usuario: Usuario){


      //TODO el rol lo estaba extrayendo para que no se cambie
      // data = {
      //   ...data,
      //   role: this.usuario.role!
      // };
  
      return this.http.put(`${base_url}/usuarios/${usuario.uid}`, usuario, this.headers);
  }
}
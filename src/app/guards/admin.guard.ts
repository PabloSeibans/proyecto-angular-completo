import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot } from '@angular/router';

import {UsuarioService} from '../services/usuario.service';
@Injectable({
  providedIn: 'root'
})
export class AdminGuard implements CanActivate {

  constructor(private usuarioService: UsuarioService,
              private router: Router){
     
  }
  canActivate(
    //Esto puede ser un operador ternario
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): boolean {
      if(this.usuarioService.role === 'ADMIN_ROLE'){
        return true;
      }else {
        //Si lo saco al login entones borro el token y el menu
        this.router.navigateByUrl('/dashboard')
        return false;
      }
      // return (this.usuarioService.role === 'ADMIN_ROLE') ? true : false;
  }
  
}

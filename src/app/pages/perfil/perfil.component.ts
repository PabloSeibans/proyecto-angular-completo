import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { UsuarioService } from 'src/app/services/usuario.service';
import Swal from 'sweetalert2';
import {Usuario} from '../../models/usuarios.model'
import { FileUploadService } from '../../services/file-upload.service'

@Component({
  selector: 'app-perfil',
  templateUrl: './perfil.component.html',
  styles: [
  ]
})
export class PerfilComponent implements OnInit {


  public perfilForm!: FormGroup;
  public usuario: Usuario;
  public imagePerfil!: File;
  public imgTemp: any = null;


  constructor(private fb:  FormBuilder,
              private usuarioService: UsuarioService,
              private fileUploadService: FileUploadService) {
                this.usuario = this.usuarioService.usuario;
              }

  ngOnInit(): void {
    this.perfilForm = this.fb.group({
      nombre: [this.usuario.nombre, Validators.required],
      email: [this.usuario.email, [Validators.required , Validators.email]],
    })
  }


  actualizarPerfil(){
    this.usuarioService.actualizarPerfil(this.perfilForm.value)
    .subscribe(
          {
            next: (resp) => {
              //TODO Aca se actualiza automaticamente porque los objetod en javascript son pasados por referencia
              const {nombre, email } = this.perfilForm.value;
              this.usuario.nombre = nombre;
              this.usuario.email = email;

              Swal.fire('Guardado', 'Cambios fueron guardados', 'success');
            },
            error: (err) => {
              Swal.fire('Ocurrio un Error', err.error.msg, 'error');
            },
            complete: () => {
              console.log('Completado Papu :V Perfil');
            }
          });
  }

  cambiarImagen(event: any){
    
    if(event.target.files[0]){
        this.imagePerfil = event.target.files[0];
        const reader = new FileReader();
        reader.readAsDataURL(event.target.files[0]);
        reader.onloadend = () => {
          this.imgTemp = reader.result;
        }
      } else {
        this.imgTemp = null;
      }
  }

  subirImagen(){
    this.fileUploadService.actualizarFoto(
      this.imagePerfil, 
      'usuarios',
      this.usuario.uid!,
      ).then(img => {
        this.usuario.img = img;
        
      }).catch(err => {
        Swal.fire('Ocurrio un Error', err.error.msg, 'error');
      });
   }

}

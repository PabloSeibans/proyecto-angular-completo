import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-promesas',
  templateUrl: './promesas.component.html',
  styles: [
  ]
})
export class PromesasComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
    this.getUsuarios().then(usuarios => {
      console.log(usuarios);
    });




    // const promise = new Promise( (resolve, reject) => {
    //   if (false) {
    //     resolve('Hola Mundo');
    //   } else {
    //     reject('Algo Salio Mal');
    //   }
    // });
    // promise
    //   .then( (mensaje) => {
    //     console.log(mensaje);
    //   })
    //   .catch(err => console.log('Error', err));
    // console.log('Fin del Init');
  }

  getUsuarios(){

    return new Promise(res => {
      fetch('https://reqres.in/api/users')
      .then( resp => resp.json())
      .then( body => res(body.data));
    });
  }

}

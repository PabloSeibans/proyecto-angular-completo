import { Component, OnInit } from '@angular/core';

import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { delay } from 'rxjs';
import { Hospital } from 'src/app/models/hospital.model';
import { Libro } from 'src/app/models/libro.model';
import { HospitalService } from 'src/app/services/hospital.service';
import Swal from 'sweetalert2';
import { LibroService } from '../../../services/libro.service';

@Component({
  selector: 'app-libro',
  templateUrl: './libro.component.html',
  styles: [
  ]
})
export class LibroComponent implements OnInit {

  public libroForm!: FormGroup;
  public hospitales: Hospital[] = [];
  public hospitalSeleccionado!: Hospital;
  public libroSeleccionado!: Libro;

  constructor(private fb: FormBuilder,
              private hospitalService: HospitalService,
              private libroService: LibroService,
              private router: Router,
              private activatedRoute: ActivatedRoute) { }

  ngOnInit(): void {

    this.activatedRoute.params
        .subscribe(({id}) => {
          this.cargarMedico(id);
        });

    

    this.libroForm = this.fb.group({
      nombre: ['', Validators.required],
      hospital: ['', Validators.required],
    });

    this.cargarHospitales();


    this.libroForm.get('hospital')?.valueChanges
        .subscribe(hospitalId => {
          this.hospitalSeleccionado = this.hospitales.find( h => h._id === hospitalId )!  
        })
    
  }


  cargarMedico(id: string){
    if(id === 'nuevo'){
      return;
    }
    this.libroService.obtenerLibroporId(id)
        .pipe(
            delay(200)
          )
        .subscribe(libro => {
          if(!libro){
            // return this.router.navigateByUrl(`/dashboard/libros`)
            this.router.navigateByUrl(`/dashboard/libros`)

          } else {
            const {nombre, hospital} = libro;
            this.libroSeleccionado = libro;
            this.libroForm.setValue({nombre, hospital: hospital?._id})
          }
      });
  }

  cargarHospitales(){
    this.hospitalService.cargarHospitales()
        .subscribe((hospitales: Hospital[]) => {
          this.hospitales = hospitales;
        })
  }

  guardarLibro(){
    const {nombre} = this.libroForm.value

    if (this.libroSeleccionado) {
      //actualizar
      const data = {
        ...this.libroForm.value,
        _id: this.libroSeleccionado._id
      }
      this.libroService.actualizarLibro( data )
          .subscribe(resp => {
            console.log(resp)
          Swal.fire('Actualizado',`${nombre} actualizado Correctamente`, 'success');
          })
      
    } else {
      this.libroService.crearLibro(this.libroForm.value)
        .subscribe((resp: any) => {
          Swal.fire('Creado',`${nombre} creado Correctamente`, 'success');
          this.router.navigateByUrl(`/dashboard/libro/${resp.libro._id}`)
        })
    }
  }
}

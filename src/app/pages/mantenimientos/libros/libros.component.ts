import { ModalImagenService } from 'src/app/services/modal-imagen.service';
import { Component, OnDestroy, OnInit } from '@angular/core';
import { Libro } from 'src/app/models/libro.model';
import { LibroService } from 'src/app/services/libro.service';
import { BusquedasService } from 'src/app/services/busquedas.service';
import { delay, Subscription } from 'rxjs';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-libros',
  templateUrl: './libros.component.html',
  styles: [
  ]
})
export class LibrosComponent implements OnInit, OnDestroy {

  public cargando: boolean = true;
  public libros: Libro[] = [];
  private imgSubs!: Subscription;

  constructor(private libroService: LibroService,
            private modalImagenService: ModalImagenService,
            private busquedasService: BusquedasService) { }

  ngOnInit(): void {
    this.cargarLibros();

    this.imgSubs = this.imgSubs = this.modalImagenService.nuevaImagen
      .pipe(
        delay(500)
      )
      .subscribe(img => {
        console.log(img)
        this.cargarLibros();
      });
  }

  ngOnDestroy(): void {
    this.imgSubs.unsubscribe();
  }

  cargarLibros(){
    this.cargando = true;
    this.libroService.cargarLibros()
      .subscribe(libros => {
        this.cargando = false;
        this.libros = libros;
        console.log(libros );
        
      })
  }


  buscar(termino: string){
    if (termino.length === 0) {
      return this.cargarLibros();
    }

    this.busquedasService.buscarL('libros', termino)
      .subscribe(resp => {
        this.libros = resp;
      })
  }

  abrirModal(libro: Libro){
    this.modalImagenService.abrirModal('libros', libro._id!, libro.img);
    
  }

  borrarLibro(libro: Libro){
    Swal.fire({
      title: 'Borrar Usuario?',
      text: `Esta a punto de Borrar a ${libro.nombre}`,
      icon: 'question',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Si Deseo Borrar el libro'
    }).then((result) => {
      if (result.value) {
        this.libroService.eliminarLibro(libro._id!)
        .subscribe(resp => {
          this.cargarLibros()
          Swal.fire(
            'Libro Borrado',
            `El Libro ${libro.nombre} fue eliminado Correctamente`,
            'success'
          )
        })
      }
    })
  }

}

import { Component, OnDestroy, OnInit } from '@angular/core';
import Swal from 'sweetalert2';

import { Usuario } from 'src/app/models/usuarios.model';

import { BusquedasService } from 'src/app/services/busquedas.service';
import { ModalImagenService } from 'src/app/services/modal-imagen.service';
import { UsuarioService } from 'src/app/services/usuario.service';
import { delay, Subscription } from 'rxjs';


@Component({
  selector: 'app-usuarios',
  templateUrl: './usuarios.component.html',
  styles: [
  ]
})
export class UsuariosComponent implements OnInit, OnDestroy {

  public totalUsuarios: number = 0;
  public usuarios: Usuario[] = [];
  public usuariosTemp: Usuario[] = [];
  public desde: number = 0;
  public cargando: boolean = true;
  public imgSubs!: Subscription;

  constructor(private usuarioService: UsuarioService,
              private busquedaService: BusquedasService,
              private modalImagenService: ModalImagenService) { }

  ngOnInit(): void {
    this.cargarUsuarios();
    this.imgSubs = this.modalImagenService.nuevaImagen
      .pipe(
        delay(500)
      )
      .subscribe(img => {
        console.log(img)
        this.cargarUsuarios()
      });
  }

  ngOnDestroy(): void {
    this.imgSubs.unsubscribe();
  }

  cargarUsuarios(){
    this.cargando = true;
    this.usuarioService.cargarUsuarios(this.desde)
    //aca esta desestructurando la respuesta
    .subscribe(({total, usuarios}) => {
      this.totalUsuarios = total;
      this.usuarios = usuarios;
      this.usuariosTemp = usuarios;
      this.cargando = false;
    });
  }

  cambiarPagina(valor: number){
    this.desde += valor;

    if(this.desde < 0){
      this.desde = 0;
    }else if(this.desde > this.totalUsuarios){
      this.desde = valor;
    }

    this.cargarUsuarios();
  }

  buscar(termino: string){

    if (termino.length === 0) {
      return this.usuarios= this.usuariosTemp;
    }

    return this.busquedaService.buscarU('usuarios',termino)
    .subscribe( resultados => {
      console.log(resultados)
      this.usuarios = resultados;
    });
  }

  eliminarUsuario(usuario: Usuario){

    if(usuario.uid === this.usuarioService.uid){
      return Swal.fire('Error','No puede borrarse a si mismo', 'error')
    }

    return Swal.fire({
      title: 'Borrar Usuario?',
      text: `Esta a punto de Borrar a ${usuario.nombre}`,
      icon: 'question',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Si Deseo Borrar al Usuario'
    }).then((result) => {
      if (result.value) {
        this.usuarioService.eliminarUsuario(usuario)
        .subscribe(resp => {
          this.cargarUsuarios()
          Swal.fire(
            'Usuario Borrado',
            `El Usuario ${usuario.nombre} fue eliminado Correctamente`,
            'success'
          )
        })
      }
    })
  }

  cambiarRole(usuario: Usuario){
    this.usuarioService.cambiarUsuario(usuario)
    .subscribe(resp => {
      // this.cargarUsuarios()
      console.log(resp)
    })
  }


  abrirModal(usuario: Usuario){
    this.modalImagenService.abrirModal('usuarios', usuario.uid!, usuario.img);
  }
}

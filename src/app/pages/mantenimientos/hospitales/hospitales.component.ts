import { HospitalService } from './../../../services/hospital.service';
import { Component, OnDestroy, OnInit } from '@angular/core';
import { Hospital } from 'src/app/models/hospital.model';
import Swal from 'sweetalert2';
import { ModalImagenService } from 'src/app/services/modal-imagen.service';
import { delay, Subscription } from 'rxjs';
import { BusquedasService } from 'src/app/services/busquedas.service';

@Component({
  selector: 'app-hospitales',
  templateUrl: './hospitales.component.html',
  styles: [
  ]
})
export class HospitalesComponent implements OnInit, OnDestroy {

  public hospitales: Hospital[] = [];
  public cargando: boolean = true;
  private imgSubs!: Subscription;

  constructor(private hospitalService: HospitalService,
    private modalImagenService: ModalImagenService,
    private busquedaService: BusquedasService) { }

  ngOnInit(): void {
    this.cargarHospitales();

    this.imgSubs = this.modalImagenService.nuevaImagen
      .pipe(
        delay(500)
      )
      .subscribe(img => {
        console.log(img)
        this.cargarHospitales()
      });
  }

  buscar(termino: string){

    if (termino.length === 0) {
      return this.cargarHospitales();
    }

    return this.busquedaService.buscarH('hospitales',termino)
    .subscribe( resp => {
      this.hospitales = resp;
    });
  }

  ngOnDestroy(): void {
    this.imgSubs.unsubscribe();
  }

  cargarHospitales(){
    this.cargando = true;
    this.hospitalService.cargarHospitales()
    .subscribe(hospitales => {
      this.cargando = false;
      this.hospitales = hospitales;
    })
  }

  guardarCambios(hospital: Hospital){
    this.hospitalService.actualizarHospital(hospital._id!, hospital.nombre)
      .subscribe(resp => {
        Swal.fire('Actualizado', hospital.nombre, 'success')
      });
  }

  eliminarHospital(hospital: Hospital){
    this.hospitalService.eliminarHospital(hospital._id!)
      .subscribe(resp => {
        this.cargarHospitales();
        Swal.fire('Actualizado', hospital.nombre, 'success')
      });
  }

  async abrirSweetAlert(){
    const {value = ''} = await Swal.fire<string>({
      title: 'Crear Hospital',
      input: 'text',
      inputLabel: 'Nombre del Hospital',
      inputPlaceholder: 'Nombre del Hospital',
      showCancelButton: true,
    })
    
    if(value.trim().length > 0 ){
      this.hospitalService.crearHospital(value!)
        .subscribe((resp: any) => {
          this.hospitales.push(resp.hospital)
        })
    }
  }

  abrirModal(hospital: Hospital){
    this.modalImagenService.abrirModal('hospitales', hospital._id!, hospital.img);
    
  }

}
   
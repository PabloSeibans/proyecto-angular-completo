import { NgModule } from '@angular/core';
import { AdminGuard } from '../guards/admin.guard';
import { Routes, RouterModule } from '@angular/router';

import { DashboardComponent } from './dashboard/dashboard.component';
import { Grafica1Component } from './grafica1/grafica1.component';
import { ProgressComponent } from './progress/progress.component';
import { AcountSettingsComponent } from './acount-settings/acount-settings.component';
import { PromesasComponent } from './promesas/promesas.component';
import { RxjsComponent } from './rxjs/rxjs.component';
import { PerfilComponent } from './perfil/perfil.component';
import { HospitalesComponent } from './mantenimientos/hospitales/hospitales.component';
import { LibrosComponent } from './mantenimientos/libros/libros.component';
import { LibroComponent } from './mantenimientos/libros/libro.component';
import { BusquedaComponent } from './busqueda/busqueda.component';
import { UsuariosComponent } from './mantenimientos/usuarios/usuarios.component';



const childRoutes: Routes = [
  
  { path: '', component: DashboardComponent, data:{title: 'Dashboard'} },
  { path: 'progress', component: ProgressComponent, data:{title: 'ProgressBar'} },
  { path: 'buscar/:termino', component: BusquedaComponent, data:{title: 'Busquedas'} },
  { path: 'grafica1', component: Grafica1Component, data:{title: 'Gráficas'} },
  { path: 'acount-settings', component: AcountSettingsComponent, data:{title: 'Ajustes'} },
  { path: 'promesas', component: PromesasComponent, data:{title: 'Promesas'} },
  { path: 'rxjs', component: RxjsComponent, data:{title: 'RxJs'} },
  { path: 'perfil', component: PerfilComponent, data:{title: 'Perfil de Usuario'} },


  //Mantenimientos
  { path: 'hospitales', component: HospitalesComponent, data:{title: 'Mantenimiento de Hospitales'} },
  { path: 'libros', component: LibrosComponent, data:{title: 'Mantenimiento de Libros'} },
  { path: 'libro/:id', component: LibroComponent, data:{title: 'Mantenimiento de Libros'} },
  
  // RUTAS DEL ADMIN si son varios guards los implementas con []
  { path: 'usuarios', canActivate: [AdminGuard], component: UsuariosComponent, data:{title: 'Usuario de Aplicación'} },

  // { path: '', redirectTo: '/dashboard', pathMatch: 'full' }
]


@NgModule({
  imports: [RouterModule.forChild(childRoutes)],
  exports: [RouterModule]
})
export class ChildRoutesModule { }

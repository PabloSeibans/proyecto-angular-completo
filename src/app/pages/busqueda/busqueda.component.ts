import { Hospital } from 'src/app/models/hospital.model';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Libro } from 'src/app/models/libro.model';
import { Usuario } from 'src/app/models/usuarios.model';
import { BusquedasService } from 'src/app/services/busquedas.service';

@Component({
  selector: 'app-busqueda',
  templateUrl: './busqueda.component.html',
  styles: [
  ]
})
export class BusquedaComponent implements OnInit {

  public usuarios: Usuario[] = [];
  public libros: Libro[] = [];
  public hospitales: Hospital[] = [];

  constructor(private activatedRoute: ActivatedRoute,
        private busquedasService: BusquedasService) { }

  ngOnInit(): void {
    this.activatedRoute.params
      .subscribe( ({termino}) => this.busquedaGlobal(termino))
  }

  busquedaGlobal(termino: string ){
    this.busquedasService.buscarTodo(termino)
        .subscribe((resp: any) => {
          this.usuarios = resp.usuarios;
          this.libros = resp.libros;
          this.hospitales = resp.hospitales;
        })
  }

}

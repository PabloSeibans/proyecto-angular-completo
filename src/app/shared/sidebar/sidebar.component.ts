import { Component, OnInit } from '@angular/core';
import { Usuario } from 'src/app/models/usuarios.model';
import { SidebarService } from 'src/app/services/sidebar.service';
import { UsuarioService } from 'src/app/services/usuario.service';

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styles: [
  ]
})
export class SidebarComponent implements OnInit {

  //TODO AQUI PASA POR REFERENCIA
  //Luego pone sidebarService.menu en el for del tml pero a mi me lee never

  public usuario: Usuario;

  public menuItems!: any[];

  constructor(public sidebarService: SidebarService,
        private usuarioService: UsuarioService
    ) {
    //Al parecer esto si es una instancia de mi clas Usuario
    //y tengo acceso a todos los headers
    this.usuario = usuarioService.usuario
  }

  ngOnInit(): void {
    this.menuItems = this.sidebarService.menu;
  }

}

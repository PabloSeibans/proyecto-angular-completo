import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Usuario } from 'src/app/models/usuarios.model';
import { UsuarioService } from 'src/app/services/usuario.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styles: [
  ]
})
export class HeaderComponent implements OnInit {

  public usuario: Usuario;

  constructor(private usuarioService: UsuarioService,
              private router: Router) {
    //Como el servicio me retorn un usuario ya con propiedades entonces
    //tiene el modelo con el get de la imagen 
    this.usuario = usuarioService.usuario;
  }

  ngOnInit(): void {
  }

  logout(){
    this.usuarioService.logout();
  }

  buscar(termino: string){
    if(termino.length === 0){
      // this.router.navigateByUrl('/dashboard')
      return;
    }
    this.router.navigateByUrl(`/dashboard/buscar/${termino}`);
  }
}

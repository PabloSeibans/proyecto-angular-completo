import { Component, OnInit } from '@angular/core';
import { FileUploadService } from 'src/app/services/file-upload.service';
import { ModalImagenService } from 'src/app/services/modal-imagen.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-modal-imagen',
  templateUrl: './modal-imagen.component.html',
  styleUrls: ['./modal-imagen.component.css']
})
export class ModalImagenComponent implements OnInit {

  public imagePerfil!: File;
  public imgTemp: any = null;

  constructor(public modalImagenService: ModalImagenService,
              public  fileUploadService: FileUploadService ) { }

  ngOnInit(): void {
  }

  cerrarModal(){
    this.imgTemp = null;
    this.modalImagenService.cerrarModal();
  }

  cambiarImagen(event: any){
    
    if(event.target.files[0]){
        this.imagePerfil = event.target.files[0];
        const reader = new FileReader();
        reader.readAsDataURL(event.target.files[0]);
        reader.onloadend = () => {
          this.imgTemp = reader.result;
        }
      } else {
        this.imgTemp = null;
      }
  }

  subirImagen(){

    const id = this.modalImagenService.id;
    const tipo = this.modalImagenService.tipo;


    this.fileUploadService.actualizarFoto(
      this.imagePerfil, tipo, id)
      .then(img => {
        //TODO este Swal no me indica si la imagen pesa mayor :()
        // Swal.fire('Guardado', 'Imagen del Usuario Actualizada', 'success');
        this.modalImagenService.nuevaImagen.emit(img);
        this.cerrarModal();
      }).catch(err => {
        Swal.fire('Ocurrio un Error', err.error.msg, 'error'); //Esto no se ve solo lo del servicio
      });
   }

}

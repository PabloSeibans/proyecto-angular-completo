import { Component, Input } from '@angular/core';
import { ChartData, ChartType,  } from 'chart.js';

@Component({
  selector: 'app-donut',
  templateUrl: './donut.component.html',
  styles: [
  ]
})
export class DonutComponent {
  @Input() title: string = 'Sin titulo';
  // Doughnut
  @Input('labels') doughnutChartLabels: string[] = [ 'Label', 'Label 2', 'Label 3' ];

  
  @Input('data') doughnutChartData: ChartData<'doughnut'> = {
    labels: this.doughnutChartLabels,
    datasets: [
      { data: [ 350, 450, 100 ] }
    ]
  };

  public doughnutChartType: ChartType = 'doughnut';

}

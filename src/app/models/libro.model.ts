import { Hospital } from './hospital.model';
interface LibroUser {
	_id: string,
	nombre: string,
	img: string,
}


export class Libro {
	 constructor(
		 public nombre: string,
		public _id?: string,
		public img?: string,
		public usuario?: LibroUser,
		public hospital?: Hospital
	 ){

	 }
}
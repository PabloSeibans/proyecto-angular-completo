import { environment } from "src/environments/environment";

const base_url = environment.base_url;

export class Usuario {
	constructor(
		public google: boolean,
		public nombre: string,
		public email: string,
		public img: string,
		public password?: string,
		//TODO Esto es solo como decirle que sera uno de esos tipos
		public role?: 'ADMIN_ROLE' | 'USER_ROLE',
		public uid?: string
	){}

	imprimirUsuario(){
		console.log(this.nombre);
	}

	get imageUrl(){
		// /upload/usuario/no-image example
		if(!this.img){
			return `${base_url}/upload/usuarios/no-image`;
		} else if (this.img.toString().includes('https')){
			return this.img;
		} else if (this.img){
			return `${base_url}/upload/usuarios/${this.img}`;
		} else {
			return `${base_url}/upload/usuarios/no-image`;
		}
	}
}